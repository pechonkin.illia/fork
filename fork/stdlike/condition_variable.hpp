#pragma once

#include <fork/wrappers/condvar.hpp>

namespace fork::stdlike {

using condition_variable = fork::ConditionVariable;  // NOLINT

}  // namespace fork::stdlike
