#pragma once

#include <fork/wrappers/mutex.hpp>

namespace fork::stdlike {

using mutex = fork::Mutex;  // NOLINT

}  // namespace fork::stdlike
