#pragma once

#include <fork/wrappers/thread.hpp>

namespace fork::stdlike {

using thread = fork::Thread;  // NOLINT

namespace this_thread {

inline void yield() {  // NOLINT
  ThisThreadYield();
}

inline auto get_id() {  // NOLINT
  return ThisThreadId();
}

}  // namespace this_thread

}  // namespace fork::stdlike
