#pragma once

#include <fork/wrappers/atomic.hpp>

namespace fork::stdlike {

template <typename T>
using atomic = fork::Atomic<T>;  // NOLINT

}  // namespace fork::stdlike
