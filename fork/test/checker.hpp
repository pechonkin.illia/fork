#pragma once

#include <fork/core/trace/description.hpp>
#include <fork/core/config.hpp>

namespace fork {

struct CheckerView {
  // Options for Explore mode

  CheckerView& AddInvariant(Invariant inv);

  using Predicate = std::function<bool()>;
  CheckerView& Prune(Predicate rule);

  CheckerView& SetUIntAtomicBound(size_t value);

  CheckerView& SetDepth(size_t depth);

  // Options for Trace mode

  CheckerView& PrintState(trace::StateDescriber describer);
  CheckerView& ShowLocals(bool flag = true);
};

CheckerView& GetChecker();

}  // namespace fork
