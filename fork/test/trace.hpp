#pragma once

#include <fork/core/trace/description.hpp>
#include <fork/core/trace/note.hpp>

#include <wheels/support/map.hpp>

namespace fork {

void ShowLocalState();
void ShowState();

}  // namespace fork

////////////////////////////////////////////////////////////////////////////////

// Automatic generation of a class description
// Usage: class MyClass {
// ...
// FORK_DESCRIBE(field1, field2, field3)
// ...
// Type1 field1;
// Type2 field2;
// Type3 field3;
// ...
// }
#define FORK_DESCRIBE(...)                        \
  trace::StateDescription DescribeState() const { \
    trace::StateDescription d;                    \
    MAP(FORK_DESCRIBE_ITEM, __VA_ARGS__)          \
    return d;                                     \
  }

#define FORK_DESCRIBE_ITEM(x) d.Add(#x, x);