#pragma once

#include <fork/test/fork.hpp>

#include <functional>
#include <string>
#include <sstream>

namespace fork {

void SetTracePath(std::string_view test_name);

using TestRoutine = std::function<void()>;
void RunTest(TestRoutine main);

void FailTest(const std::string& reason);

}  // namespace fork

////////////////////////////////////////////////////////////////////////////////

#define FORK_TEST(name)        \
  void TestBody();             \
  void ForkTest() {            \
    fork::SetTracePath(#name); \
    TestBody();                \
  }                            \
  int main() {                 \
    fork::RunTest(ForkTest);   \
    return 0;                  \
  }                            \
  void TestBody()

////////////////////////////////////////////////////////////////////////////////

#define FORK_ASSERT(condition, message) \
  do {                                  \
    fork::Fork();                       \
    if (!(condition)) {                 \
      std::stringstream s;              \
      s << message;                     \
      fork::FailTest(s.str());          \
    }                                   \
  } while (false)
