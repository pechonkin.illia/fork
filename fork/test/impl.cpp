#include <fork/test/test.hpp>
#include <fork/test/trace.hpp>

#include <fork/core/checker.hpp>
#include <fork/core/trace/trace_file.hpp>

#include <wheels/support/env.hpp>

#include <string>
#include <sstream>

namespace fork {

/////////////////////////////////////////////////////////////////////

// trace.hpp

#if defined(FORK_TRACE)

void ShowNote(const std::string& note, bool internal) {
  if (!IsRunningInChecker() ||
      (internal && GetCurrentChecker()->ForksDisabled())) {
    return;
  }
  GetCurrentChecker()->GetTracer().ShowNote(note);
}

void ShowLocalState() {
  if (!IsRunningInChecker() || GetCurrentChecker()->ForksDisabled()) {
    return;
  }
  GetCurrentChecker()->GetTracer().ShowLocalState();
}

void ShowState() {
  if (!IsRunningInChecker() || GetCurrentChecker()->ForksDisabled()) {
    return;
  }
  GetCurrentChecker()->GetTracer().ShowState();
}

#else

void ShowNote(const std::string&) {
}

void ShowLocalState() {
}

void ShowState() {
}

#endif

/////////////////////////////////////////////////////////////////////

// random.hpp

size_t RandomNumber(size_t max) {
  size_t number = GetCurrentChecker()->Random(max);
  FORK_INTERNAL_NOTE("Random", "RandomNumber(" << max << ") -> " << number);
  return number;
}

/////////////////////////////////////////////////////////////////////

// test.hpp

void SetTracePath(std::string_view test_name) {
  GetCurrentChecker()->SetTracePath(trace::TraceFile(test_name));
}

void RunTest(TestRoutine main) {
  Checker checker;
  checker.Run(&main);
}

void FailTest(const std::string& reason) {
  GetCurrentChecker()->Fail(reason);
}

}  // namespace fork
