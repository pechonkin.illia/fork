#pragma once

#include <wheels/support/preprocessor.hpp>

#include <utility>

namespace fork {

// Enqueues new state into the checker queue
void Fork();

// RAII object that turns forks off
class ForkGuard {
 public:
  ForkGuard(bool forks_disabled = true);
  ~ForkGuard();

 private:
  bool prev_disabled_;
};

// Wraps object methods making them atomic
// Usage:
// ForkGuarded<SomeObj> obj_;
// obj_->ObjMethod();
template <typename T>
class ForkGuarded {
 private:
  class Guarded : public ForkGuard {
   public:
    explicit Guarded(T& object) : object_(object) {
    }

    T* operator->() {
      return &object_;
    }

   private:
    T& object_;
  };

 public:
  template <typename... Args>
  ForkGuarded(Args&&... args) : object_(std::forward<Args>(args)...) {
  }

  Guarded operator->() {
    Fork();
    return Guarded(object_);
  }

 private:
  T object_;
};

template <typename T>
class ForkNotGuarded {
 public:
  template <typename... Args>
  ForkNotGuarded(Args&&... args) : object_(std::forward<Args>(args)...) {
  }

  T* operator->() {
    return &object_;
  }

 private:
  T object_;
};

}  // namespace fork

#define __WITH_FORKS(flag)                               \
  if (fork::ForkGuard UNIQUE_NAME(__fork){flag}; true) { \
    goto CONCAT(__label, __LINE__);                      \
  } else                                                 \
    CONCAT(__label, __LINE__) :

// Usage:
// ATOMICALLY {
//  <code that executes atomically>
// }
#define FORK_ATOMICALLY __WITH_FORKS(true)

// Undoes effects of any Fork guards
#define FORCE_FORKS __WITH_FORKS(false)
