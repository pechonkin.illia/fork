#include <fork/core/checker.hpp>

#include <fork/test/checker.hpp>

namespace fork {

static CheckerView checker;

CheckerView& GetChecker() {
  return checker;
}

CheckerView& CheckerView::AddInvariant(Invariant inv) {
  GetCurrentChecker()->AddInvariant(inv);
  return *this;
}

CheckerView& CheckerView::Prune(Predicate rule) {
  GetCurrentChecker()->AddPruneRule(std::move(rule));
  return *this;
}

CheckerView& CheckerView::PrintState(trace::StateDescriber describer) {
  GetCurrentChecker()->PrintState(std::move(describer));
  return *this;
}

CheckerView& CheckerView::ShowLocals(bool flag) {
  GetCurrentChecker()->GetTracer().ShowLocals(flag);
  return *this;
}

CheckerView& CheckerView::SetDepth(size_t depth) {
  GetCurrentChecker()->SetDepth(depth);
  return *this;
}

}  // namespace fork