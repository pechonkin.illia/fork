#pragma once

#include <cstdlib>
#include <cassert>

namespace fork {

// Generates max+1 execution states
// Returns value \in [0, max]
size_t RandomNumber(size_t max);

// Returns value \in [lo, hi]
inline size_t RandomNumber(size_t lo, size_t hi) {
  assert(lo <= hi);
  return lo + RandomNumber(hi - lo);
}

// ~ RandomNumber(1)
// Usage:
// if (Either()) {
// ...
// } else {
// ...
// }
inline bool Either() {
  return RandomNumber(/*max=*/1) == 1;
}

}  // namespace fork
