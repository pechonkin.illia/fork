#pragma once

#include <fork/test/test.hpp>

namespace fork {

class SharedObject {
 public:
  void ExclusiveAccess() {
    bool accessed = std::exchange(accessed_, true);
    FORK_ASSERT(!accessed, "Mutual exclusion violated");
    accessed_ = false;
  }

  FORK_DESCRIBE(accessed_);

 private:
  bool accessed_ = false;
};

}  // namespace fork