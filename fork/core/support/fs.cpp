#include <fork/core/support/fs.hpp>

#include <sys/stat.h>
#include <sys/types.h>

namespace fork::support {

// todo: to wheels?
static bool CreateDir(const std::string& path) {
  return mkdir(path.data(), 0700) == 0;
}

// todo: to wheels?
void CreateDirectories(const std::string& path) {
  size_t pos = 0;
  pos = path.find('/', pos + 1);

  while (pos != std::string::npos) {
    CreateDir(path.substr(0, pos));
    pos = path.find('/', pos + 1);
  }
}

}  // namespace fork::support