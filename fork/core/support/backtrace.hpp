#pragma once

#include <string>
#include <vector>

void LoadBacktrace(std::string& object, std::vector<std::string>& functions,
                   std::vector<std::string>& traces);
