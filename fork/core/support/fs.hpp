#pragma once

#include <string>

namespace fork::support {

void CreateDirectories(const std::string& path);

}  // namespace fork::support
