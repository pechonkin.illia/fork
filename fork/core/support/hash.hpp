#pragma once

#include <cstddef>
#include <cassert>
#include <cstdint>
#include <cstring>

namespace hash {

using THash = uint64_t;

class Hasher {
 private:
  static const THash kPrime = 0x100000001b3;

 public:
  explicit Hasher(THash init = 0xcbf29ce484222325) : hash_(init) {
  }

  template <typename T>
  Hasher& Absorb(const T* data, size_t bytes) {
    const auto* hash_data = reinterpret_cast<const THash*>(data);
    size_t hash_size = bytes >> 3;
    size_t i = 0;
    for (; i + 7 < hash_size; i += 8) {
      hash_ = kPrime * kPrime * kPrime * kPrime * kPrime * kPrime * kPrime *
                  kPrime * hash_ ^
              kPrime * kPrime * kPrime * kPrime * kPrime * kPrime * kPrime *
                  hash_data[i] ^
              kPrime * kPrime * kPrime * kPrime * kPrime * kPrime *
                  hash_data[i + 1] ^
              kPrime * kPrime * kPrime * kPrime * kPrime * hash_data[i + 2] ^
              kPrime * kPrime * kPrime * kPrime * hash_data[i + 3] ^
              kPrime * kPrime * kPrime * hash_data[i + 4] ^
              kPrime * kPrime * hash_data[i + 5] ^ kPrime * hash_data[i + 6] ^
              hash_data[i + 7];
    }
    for (; i < hash_size; i++) {
      hash_ = kPrime * hash_ ^ hash_data[i];
    }
    if ((bytes & 7) != 0) {
      THash last = 0;
      memcpy(&last, hash_data + hash_size, bytes & 7);
      hash_ = kPrime * hash_ ^ last;
    }
    return *this;
  }

  template <typename T>
  Hasher& Absorb(const T& object) {
    object.FeedHasher(*this);
    return *this;
  }

  THash Hash() const {
    return hash_;
  }

 private:
  THash hash_;
};

}  // namespace hash
