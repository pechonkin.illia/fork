#pragma once

#include <fork/core/storage/disk_based.hpp>
#include <fork/core/support/hash.hpp>

#include <wheels/support/memspan.hpp>

#include <unordered_set>

namespace storage {

class PointerStorage {
 public:
  void Clear() {
    pointers_.clear();
    saved_ptrs_ = 0;
  }

  void Assign(wheels::MemSpan& dest, wheels::MemSpan source) {
    auto pointer = pointers_.find(source);
    if (pointer == pointers_.end()) {
      dest = {DiskAllocate(source.Size()), source.Size()};
      memcpy(dest.Data(), source.Data(), source.Size());
      pointers_.insert(dest);
    } else {
      dest = *pointer;
      ++saved_ptrs_;
    }
  }

  size_t Saved() const {
    return saved_ptrs_;
  }

 private:
  struct Hasher {
    size_t operator()(wheels::MemSpan data) const {
      return hash::Hasher().Absorb(data.Data(), data.Size()).Hash();
    }
  };

  struct Comparator {
    bool operator()(wheels::MemSpan left, wheels::MemSpan right) const {
      if (left.Size() != right.Size()) {
        return false;
      }
      return memcmp(left.Data(), right.Data(), left.Size()) == 0;
    }
  };

 private:
  std::unordered_set<wheels::MemSpan, Hasher, Comparator> pointers_;
  size_t saved_ptrs_{0};
};

}  // namespace storage
