#pragma once

#include <functional>
#include <string>
#include <tuple>

namespace fork {

using PruneRule = std::function<bool()>;

using InvCheckResult = std::tuple<bool, std::string>;
using Invariant = std::function<InvCheckResult()>;

}  // namespace fork
