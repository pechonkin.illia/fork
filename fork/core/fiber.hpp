#pragma once

#include <fork/core/wait_queue.hpp>
#include <fork/core/storage/disk_based.hpp>
#include <fork/core/support/hash.hpp>

#include <context/context.hpp>
#include <context/stack.hpp>

#include <wheels/support/time.hpp>

#include <utility>
#include <cassert>
#include <cstring>
#include <vector>
#include <functional>

namespace fork {

using FiberId = size_t;

extern const FiberId kInvalidFiberId;

class Fiber;

using FiberPtrs = std::vector<Fiber*>;

using FiberRoutine = std::function<void()>;
// TODO
using FiberRoutinePtr = FiberRoutine*;

//////////////////////////////////////////////////////////////////////

enum class FiberState : int32_t {
  Starting,
  Runnable,
  Running,
  Terminated,
  Restarted,
  Suspended
};

std::string_view StateToString(FiberState state);

inline bool StateEnabled(FiberState state) {
  return state != FiberState::Terminated && state != FiberState::Suspended;
}

//////////////////////////////////////////////////////////////////////

// NB: Should be trivially copyable
struct FiberFields {
  WaitQueue* wait_queue_{nullptr};
  FiberState state_;
  int32_t random_{-1};
};

//////////////////////////////////////////////////////////////////////

class Fiber {
 public:
  Fiber(FiberId id, context::Stack stack);

  FiberId Id() const {
    return id_;
  }

  const context::Stack& GetStack() const {
    return stack_;
  }

  context::ExecutionContext& Context() {
    return context_;
  }

  FiberState State() const {
    return this_.state_;
  }

  void SetState(FiberState target) {
    this_.state_ = target;
  }

  void SetWaitQueue(WaitQueue* wq) {
    this_.wait_queue_ = wq;
  }

  ::fork::WaitQueue* WaitQueue() const {
    return this_.wait_queue_;
  }

  FiberRoutinePtr UserRoutinePtr() {
    return routine_ptr_;
  }

  FiberRoutine UserRoutine() __attribute__((noinline)) {
    return *routine_ptr_;
  }

  void InvokeRoutine() {
    (*routine_ptr_)();
  }

  void SetUserRoutinePtr(FiberRoutinePtr routine_ptr) {
    routine_ptr_ = routine_ptr;
  }

  size_t StackSize() const {
    return stack_end_ - (char*)context_.machine_ctx_.rsp_;
  }

  bool Enabled() const {
    return StateEnabled(this_.state_);
  }

  void Init(FiberRoutinePtr routine_ptr);

  static Fiber* Create(FiberRoutinePtr routine_ptr);
  static void SetupTrampoline(Fiber* fiber);

  hash::THash Hash(bool symmetric);
  void FeedHasher(hash::Hasher& hasher, bool symmetric = false) const;
  char* CanonizeStack() const;

  //  void PrintStack() const {
  //    Print(context_.machine_ctx_.rsp_, StackSize());
  //  }
  //
  //  void Print(void* stack, size_t size) const {
  //    std::cout << this << std::endl;
  //    uint64_t* data = (uint64_t*)stack;
  //    for (size_t i = 0; i < size / 8; ++i) {
  //      std::cout << std::hex << data[i] << " ";
  //    }
  //    std::cout << std::dec;
  //    std::cout << std::endl;
  //  }

  // Random

  // Set bound / generated random number
  void SetRandom(int32_t random) {
    this_.random_ = random;
  }

  int32_t GetRandom() const {
    return this_.random_;
  }

  size_t ConsumeRandomNumber() {
    int32_t number = std::exchange(this_.random_, -1);
    assert(number >= 0);
    return number;
  }

  void CanonizeValues(context::StackView values) {
    to_canonize_ = values;
  }

  FiberId& Initiator() {
    return initiator_;
  }

  class Snapshot;
  Snapshot CreateSnapshot();
  void Restore(const Snapshot& snapshot);

 public:
  class Snapshot {
   public:
    Snapshot(Snapshot&& other) {
      this_ = other.this_;
      stack_ = other.stack_;
      other.stack_ = {};
    }

    Snapshot& operator=(Snapshot&& other) = default;
    Snapshot& operator=(const Snapshot& other) = default;
    Snapshot(const Snapshot& other) = default;

    ~Snapshot() {
      storage::DiskFree(stack_.Data());
    }

    size_t Size() const {
      return stack_.Size();
    }

    bool Enabled() const {
      return StateEnabled(this_.state_);
    }

    Snapshot() = default;

   private:
    context::StackView stack_;
    FiberFields this_;

    FiberRoutinePtr routine_ptr_;

    friend Fiber;
  };

 private:
  context::Stack stack_;
  FiberFields this_;

  FiberRoutinePtr routine_ptr_;

  FiberId id_;
  context::ExecutionContext context_;
  char* stack_end_;

  context::StackView to_canonize_{};

  FiberId initiator_{0};
};

}  // namespace fork
