#pragma once

#include <fork/core/fiber.hpp>
#include <fork/core/state.hpp>
#include <fork/core/memory/allocator.hpp>
#include <fork/core/state_queue.hpp>
#include <fork/core/config.hpp>
#include <fork/core/storage/snapshot_storage.hpp>
#include <fork/core/trace/tracer.hpp>
#include <fork/core/trace/trace.hpp>
#include <fork/core/support/hash.hpp>

#include <wheels/support/sequencer.hpp>
#include <wheels/support/env.hpp>

#include <vector>
#include <optional>

namespace fork {

class Checker {
 public:
  void Run(FiberRoutinePtr main);
  void Setup();
  // Modes
  void Explore();

  // Fiber syscalls

  void Yield();
  void Suspend(WaitQueue* wait_queue);
  void Resume(Fiber* waiter);
  void Terminate();

  void Spawn(FiberRoutinePtr function);

  size_t Random(size_t max);

  void* Allocate(size_t size, bool faulty = false);
  void Free(void* pointer, bool faulty = false);

  void Fork();

  // FORK_ASSERT
  void Fail(std::string reason);

  FiberPtrs GetWaiters(const WaitQueue* wq) const;

  void RestartFiber();

  void AddPruneRule(PruneRule rule);
  void AddInvariant(Invariant invariant);
  void SetDepth(size_t depth);

  // ForkGuarded
  bool DisableForks(bool disable) {
    return std::exchange(forks_disabled_, disable);
  }

  storage::PointerStorage& StackStorage() {
    return states_.StackStorage();
  }

  storage::PointerStorage& AllocatorStorage() {
    return states_.AllocatorStorage();
  }

  void PrintState(trace::StateDescriber printer) {
    tracer_.AddPrinter(std::move(printer));
  }

  void SetTracePath(const std::string& path);

  trace::Tracer& GetTracer() {
    return tracer_;
  }

  bool ForksDisabled() const {
    return forks_disabled_;
  }

  size_t NextId() {
    return sequencer_.Next();
  }

 private:
  void Init(FiberRoutinePtr main);

  void FailTest(trace::Trace trace, std::string failure);
  void CompleteTest();

  void Step(State& prev, size_t index);
  void Restore(const State& prev);
  void ProcessNewState(State& prev, trace::Step step);

  // true - pruned, false - ok
  bool CheckPruneRules();
  // false - invariant failed
  bool CheckInvariants(std::string& failure);

  State Snapshot(StatePath path) const;

  InvCheckResult CheckDeadlock();

  void SwitchTo(Fiber* fiber);
  void SwitchToChecker(bool final = false);

  void ReportStats() const;

  hash::THash HashState(bool symmetric) const;

  void Trace(FiberRoutinePtr main);

  trace::Trace ReadViolationTrace() const;
  void WriteViolationTrace(const trace::Trace& path);

 private:
  // Active execution state
  FiberPtrs fibers_;
  size_t fiber_count_{0};
  MemoryAllocator memory_allocator_;

  // Checker execution context
  context::ExecutionContext loop_context_;

  // States queue
  storage::SnapshotStorage states_;

  std::vector<Invariant> invariants_;

  std::vector<PruneRule> prune_rules_;
  size_t pruned_count_{0};

  std::optional<std::string> failure_;

  size_t depth_{0};

  wheels::StopWatch<> timer_;
  // todo: add exponential stats printer?
  wheels::StopWatch<> heartbeat_;

  size_t max_depth_{static_cast<size_t>(-1)};

  bool forks_disabled_{false};

  // todo: path and Read/Write path to Tracer
  std::optional<std::string> trace_fpath_;

  trace::Tracer tracer_{fibers_};

  wheels::Sequencer sequencer_;
};

//////////////////////////////////////////////////////////////////////

Fiber* GetCurrentFiber();
Checker* GetCurrentChecker();

bool IsRunningInChecker();

//////////////////////////////////////////////////////////////////////

// Shortcuts

void Spawn(FiberRoutine routine);

FiberId GetThisFiberId();

void Fork();

size_t Random(size_t max);

/////////////////////////////////////////////////////////////////////

// Access checker from fiber
class CheckerGuard {
 public:
  CheckerGuard();
  ~CheckerGuard();

 private:
  Fiber* fiber_;
};

}  // namespace fork
