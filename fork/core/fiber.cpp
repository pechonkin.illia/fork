#include <fork/core/fiber.hpp>
#include <fork/core/checker.hpp>
#include <fork/core/storage/disk_based.hpp>

#include <context/stack.hpp>

#include <wheels/support/compiler.hpp>
#include <wheels/support/sequencer.hpp>

namespace fork {

const FiberId kInvalidFiberId = (size_t)-1;

std::string_view StateToString(FiberState state) {
  switch (state) {
    case FiberState::Starting:
      return "STARTING";
    case FiberState::Runnable:
      return "RUNNABLE";
    case FiberState::Running:
      return "RUNNING";
    case FiberState::Terminated:
      return "TERMINATED";
    case FiberState::Restarted:
      return "RESTARTED";
    case FiberState::Suspended:
      return "SUSPENDED";
    default:
      WHEELS_UNREACHABLE();
  }
}

//////////////////////////////////////////////////////////////////////

static FiberId GenerateId() {
  return GetCurrentChecker()->NextId();
}

Fiber* Fiber::Create(FiberRoutinePtr routine_ptr) {
  FiberId id = GenerateId();

  static const size_t kStackSizePages = 8;
  auto stack = context::Stack::AllocatePages(kStackSizePages);

  auto* fiber = new Fiber(id, std::move(stack));
  fiber->Init(routine_ptr);

  return fiber;
}

Fiber::Fiber(FiberId id, context::Stack stack)
    : stack_(std::move(stack)), id_(id) {
  stack_end_ = stack_.View().End();
}

void Fiber::Init(FiberRoutinePtr routine_ptr) {
  routine_ptr_ = routine_ptr;
  SetState(FiberState::Starting);

  SetupTrampoline(this);
}

//////////////////////////////////////////////////////////////////////

static void FiberTrampoline() {
  Fiber* self = GetCurrentFiber();
  self->SetState(FiberState::Running);
  self->Context().AfterStart();

  try {
    self->InvokeRoutine();
  } catch (...) {
    WHEELS_PANIC("Unhandled exception in thread T" << self->Id());
  }

  GetCurrentChecker()->Terminate();  // never returns

  WHEELS_UNREACHABLE();
}

void Fiber::SetupTrampoline(Fiber* fiber) {
  fiber->context_.Setup(
      /*stack=*/fiber->stack_.View(),
      /*trampoline=*/FiberTrampoline);
}

//////////////////////////////////////////////////////////////////////

Fiber::Snapshot Fiber::CreateSnapshot() {
  Snapshot snapshot;

  // Save stack contents
  size_t stack_size = StackSize();
  wheels::MemSpan stack{static_cast<char*>(context_.machine_ctx_.rsp_),
                        stack_size};
  GetCurrentChecker()->StackStorage().Assign(snapshot.stack_, stack);

  // Save other fields
  memcpy(&snapshot.this_, &this_, sizeof(FiberFields));

  snapshot.routine_ptr_ = routine_ptr_;

  return snapshot;
}

void Fiber::Restore(const Snapshot& snapshot) {
  context_.machine_ctx_.rsp_ = stack_end_ - snapshot.stack_.Size();

  // Restore stack contents
  memcpy(context_.machine_ctx_.rsp_, snapshot.stack_.Begin(),
         snapshot.stack_.Size());

  // Restore other fields
  memcpy(&this_, &snapshot.this_, sizeof(FiberFields));

  routine_ptr_ = snapshot.routine_ptr_;
}

//////////////////////////////////////////////////////////////////////

void Fiber::FeedHasher(hash::Hasher& hasher, bool symmetric) const {
  char* stack = symmetric ? CanonizeStack()
                          : static_cast<char*>(context_.machine_ctx_.rsp_);

  /*std::cout << std::hex << routine_ptr_ << std::endl;
  Print(stack, StackSize());
  PrintStack();
  std::cout << std::endl;*/

  hasher.Absorb(stack, StackSize());
  hasher.Absorb(&this_, sizeof(FiberFields));
}

// Get rid of:
// a) stack addresses
// b) fiber pointers
static const size_t kStackSize = 8 * 4096;
static char stack_storage[kStackSize];
char* Fiber::CanonizeStack() const {
  size_t stack_size = StackSize();
  assert(stack_size <= kStackSize);
  memcpy(stack_storage, context_.machine_ctx_.rsp_, stack_size);
  auto* stack = reinterpret_cast<uint64_t*>(stack_storage);

  const auto rsp = reinterpret_cast<uint64_t>(context_.machine_ctx_.rsp_);
  uint64_t bottom = rsp + stack_size;

  auto fiber_begin = reinterpret_cast<uint64_t>(this);
  uint64_t fiber_end = fiber_begin + sizeof(Fiber) * 8;

  for (size_t i = 0; i < stack_size; i += 8, ++stack) {
    if (*stack <= bottom && *stack >= rsp) {
      *stack -= rsp;
    } else if (*stack >= fiber_begin && *stack < fiber_end) {
      // std::cout << "FIBER PTR: " << std::hex << *stack << std::dec <<
      // std::endl;
      *stack -= fiber_begin;
    } else if (*stack == (uint64_t)routine_ptr_) {
      *stack = 0;
    } else if (*stack >= (uint64_t)to_canonize_.Begin() &&
               *stack < (uint64_t)to_canonize_.End()) {
      *stack = 0;
    }
  }

  return stack_storage;
}

hash::THash Fiber::Hash(bool symmetric) {
  hash::Hasher hasher;
  FeedHasher(hasher, symmetric);
  return hasher.Hash();
}

}  // namespace fork
