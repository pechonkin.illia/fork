#pragma once

#include <memory>
#include <string>

namespace fork {

class WaitQueue {
 public:
  WaitQueue(std::string descr) : descr_(std::move(descr)) {
  }

  void Park();

  void WakeOne();
  void WakeAll();

  std::string_view Describe() const {
    return descr_;
  }

  std::string ToString() const;

 private:
  std::string descr_;
};

}  // namespace fork
