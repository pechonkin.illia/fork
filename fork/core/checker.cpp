#include <fork/core/checker.hpp>
#include <fork/core/storage/disk_based.hpp>
#include <fork/test/fork.hpp>

#include <fork/core/trace/trace_file.hpp>

#include <wheels/support/assert.hpp>
#include <wheels/support/terminal.hpp>
#include <wheels/support/exception.hpp>
#include <wheels/logging/logging.hpp>

#include <cstring>
#include <fstream>

namespace fork {

//////////////////////////////////////////////////////////////////////

static thread_local Fiber* current_fiber;

Fiber* GetCurrentFiber() {
  return current_fiber;
}

static inline Fiber* GetAndResetCurrentFiber() {
  auto* f = current_fiber;
  current_fiber = nullptr;
  return f;
}

static inline void SetCurrentFiber(Fiber* f) {
  current_fiber = f;
}

//////////////////////////////////////////////////////////////////////

static thread_local Checker* current_checker;

Checker* GetCurrentChecker() {
  WHEELS_VERIFY(current_checker, "not in model checker");
  return current_checker;
}

bool IsRunningInChecker() {
  return current_fiber != nullptr;
}

struct CheckerScope {
  explicit CheckerScope(Checker* checker) {
    WHEELS_VERIFY(!current_checker,
                  "cannot run model checker from another model checker");
    current_checker = checker;
  }

  ~CheckerScope() {
    current_checker = nullptr;
  }
};

void Checker::Run(FiberRoutinePtr main) {
  CheckerScope scope(this);
  Setup();
  try {
#if defined(FORK_TRACE)
    Trace(main);
#else
    Init(main);
    Explore();
#endif
  } catch (...) {
    WHEELS_PANIC(
        "Checker internal error: " << wheels::CurrentExceptionMessage());
  }
}

void Checker::Explore() {
  timer_.Restart();
  heartbeat_.Restart();

  while (states_.HasMore() && depth_ < max_depth_) {
    State state = states_.Pop();

    size_t depth = state.Depth();
    if (depth > depth_) {
      depth_ = depth;
      storage::DiskReleaseOld();

      if (heartbeat_.Elapsed() >= std::chrono::seconds(1)) {
        heartbeat_.Restart();
        ReportStats();
      }

      states_.ForgetFront();
    }

    size_t fiber_count = state.FiberCount();
    for (size_t i = 0; i < fiber_count; ++i) {
      if (state.Enabled(i)) {
        Step(state, i);
      }
    }
  }

  CompleteTest();
}

InvCheckResult Checker::CheckDeadlock() {
  bool finished = true;
  bool continuable = false;
  std::stringstream report;
  report << "Deadlock detected, suspended threads: ";

  for (auto fiber : fibers_) {
    auto state = fiber->State();
    if (state != FiberState::Terminated) {
      finished = false;
    }
    if (fiber->Enabled()) {
      continuable = true;
      break;
    }
    if (fiber->State() == FiberState::Suspended) {
      report << fiber->Id() << " ";
    }
  }

  return {finished || continuable, report.str()};
}

void Checker::Init(FiberRoutinePtr main) {
  Spawn(main);
  states_.Push(Snapshot({}));
}

void Checker::Spawn(FiberRoutinePtr function) {
  static const size_t kThreadsLimit = 10;

  assert(fiber_count_ + 1 < kThreadsLimit);

  if (fiber_count_ < fibers_.size()) {
    fibers_[fiber_count_]->Init(function);
  } else {
    Fiber* fiber = Fiber::Create(function);
    fibers_.push_back(fiber);
  }
  ++fiber_count_;
}

State Checker::Snapshot(StatePath path) const {
  return State::Create(fibers_, fiber_count_, memory_allocator_, path);
}

void Checker::Step(State& prev, size_t index) {
  Restore(prev);
  SwitchTo(fibers_[index]);

  int32_t random = fibers_[index]->GetRandom();
  if (random == -1) {
    ProcessNewState(prev, trace::JustStep(index));
  } else {
    State fork = Snapshot(prev.Path());

    for (int32_t number = 0; number <= random; ++number) {
      fibers_[index]->SetRandom(number);
      SwitchTo(fibers_[index]);
      ProcessNewState(fork, trace::RandomStep(index, number));
      Restore(fork);
    }
  }
}

void Checker::Yield() {
  if (!IsRunningInChecker()) {
    return;
  }
  Fiber* caller = GetCurrentFiber();
  caller->SetState(FiberState::Runnable);
  SwitchToChecker();
}

void Checker::Suspend(WaitQueue* wait_queue) {
  Fiber* caller = GetCurrentFiber();
  caller->SetWaitQueue(wait_queue);
  caller->SetState(FiberState::Suspended);

  // FORK_INTERNAL_NOTE("Park fiber " << fork::GetCurrentFiber()->Id() << " ("
  // << Info(ptr) << ")");

  SwitchToChecker();
}

void Checker::Resume(Fiber* waiter) {
  waiter->SetWaitQueue(nullptr);
  waiter->SetState(FiberState::Runnable);
}

void Checker::Terminate() {
  Fiber* caller = GetCurrentFiber();
  caller->SetState(FiberState::Terminated);
  SwitchToChecker();
}

void Checker::RestartFiber() {
  Fiber* caller = GetCurrentFiber();
  caller->SetState(FiberState::Restarted);
  SwitchToChecker();
}

void Checker::SwitchTo(Fiber* fiber) {
  assert(fiber->Enabled());

  SetCurrentFiber(fiber);
  fiber->SetState(FiberState::Running);

  loop_context_.SwitchTo(fiber->Context());

  if (fiber->State() == FiberState::Restarted) {
    size_t bytes = 1024;
    memset(fiber->GetStack().View().Back() - bytes, 0, bytes);
    fiber->Init(fiber->UserRoutinePtr());
    SetCurrentFiber(fiber);
    fiber->SetState(FiberState::Running);
    loop_context_.SwitchTo(fiber->Context());
  }
}

void Checker::SwitchToChecker(bool final) {
#if defined(FORK_TRACE)
  tracer_.TraceAfter();
#endif

  // Need to force forks here: imagine ForkGuarded blocking queue:
  // if it tries to lock already taken mutex, it has to switch to checker
  FORCE_FORKS {
    Fiber* caller = GetAndResetCurrentFiber();
    if (final) {
      caller->Context().ExitTo(loop_context_);
    } else {
      caller->Context().SwitchTo(loop_context_);
    }
  }

#if defined(FORK_TRACE)
  tracer_.TraceBefore();
#endif
}

bool Checker::CheckInvariants(std::string& failure_out) {
  if (failure_) {
    failure_out = *failure_;
    return false;
  }
  for (auto& inv : invariants_) {
    if (auto [ok, failure] = inv(); !ok) {
      failure_out = failure;
      return false;
    }
  }
  return true;
}

bool Checker::CheckPruneRules() {
  for (auto& prune : prune_rules_) {
    if (prune()) {
      return true;
    }
  }
  return false;
}

trace::Trace GetCurrentTrace(State& prev, trace::Step last) {
  auto trace = prev.Path().ToVector();
  trace.push_back(last);
  return trace;
}

void Checker::FailTest(trace::Trace trace, std::string failure) {
  ReportStats();
  std::cout << RED("Invariant violated: " << failure) << std::endl;

  std::cout << "Trace: ";
  for (auto step : trace) {
    std::cout << 'T' << (size_t)step.fiber_index;
    if (step.random_number >= 0) {
      std::cout << "/R" << (size_t)step.random_number;
    }
    std::cout << " ";
  }
  std::cout << "(length: " << trace.size() << ")" << std::endl;

  WriteViolationTrace(trace);
  std::cout << "Violation trace written to " << GREEN(*trace_fpath_)
            << std::endl;
  std::exit(1);
}

void Checker::CompleteTest() {
  ReportStats();

  std::cout << "Exploration completed" << std::endl;
}

void Checker::ProcessNewState(State& prev, trace::Step step) {
  // Check invariants
  std::string failure;
  if (!CheckInvariants(failure)) {
    FailTest(GetCurrentTrace(prev, step), failure);  // Never returns
  }

  // Prune
  if (CheckPruneRules()) {
    pruned_count_++;
    return;
  }

  // Add to queue
  auto state_hash = HashState(true);
  // std::cout << "hash: " << state_hash << std::endl;
  if (!states_.Contains(state_hash)) {
    // std::cout << "new" << std::endl;
    states_.Push(Snapshot(prev.NewPath(step)));
  } else {
    // std::cout << "dup" << std::endl;
  }
}

void* Checker::Allocate(size_t size, bool faulty) {
  if (faulty) {
    Fork();
  }
  return memory_allocator_.Allocate(size);
}

void Checker::Free(void* pointer, bool faulty) {
  if (faulty) {
    Fork();
  }
  memory_allocator_.Free(pointer);
}

FiberPtrs Checker::GetWaiters(const WaitQueue* wq) const {
  FiberPtrs waiters;
  for (size_t i = 0; i < fiber_count_; ++i) {
    if (fibers_[i]->WaitQueue() == wq) {
      waiters.push_back(fibers_[i]);
    }
  }
  return waiters;
}

void Checker::Setup() {
  memory_allocator_.Init();
  storage::DiskInit();

  AddInvariant([this]() { return CheckDeadlock(); });
}

void Checker::Fail(std::string reason) {
  failure_.emplace(reason);
  Terminate();
}

void Checker::Restore(const State& prev) {
  fiber_count_ = prev.FiberCount();

  for (size_t i = 0; i < fiber_count_; ++i) {
    fibers_[i]->Restore(prev.Fibers()[i]);
  }

  memory_allocator_.Restore(prev.AllocatorSnapshot());
}

void Checker::WriteViolationTrace(const trace::Trace& trace) {
  trace::WriteTrace(*trace_fpath_, trace);
}

trace::Trace Checker::ReadViolationTrace() const {
  return trace::ReadTrace(*trace_fpath_);
}

void Checker::Trace(FiberRoutinePtr main) {
  Spawn(main);

  while (Fiber* fiber = tracer_.Next()) {
    SwitchTo(fiber);

    std::string failure;
    if (!CheckInvariants(failure)) {
      tracer_.Finish(failure);
      std::exit(0);
    }
  }

  std::cout << "Violation not found" << std::endl;
  std::exit(1);
}

void PushDown(std::vector<hash::THash>& hashes, size_t j) {
  while (j > 0 && hashes[j] < hashes[j - 1]) {
    std::swap(hashes[j], hashes[j - 1]);
    --j;
  }
}

hash::THash Checker::HashState(bool symmetric) const {
  hash::Hasher hasher;

  static std::vector<hash::THash> hashes(4);

  if (symmetric) {
    hashes.resize(fiber_count_);

    for (size_t i = 0; i < fiber_count_; ++i) {
      // std::cout << i << std::endl;
      hashes[i] = fibers_[i]->Hash(true);
      PushDown(hashes, i);
    }

    hasher.Absorb(hashes.data(), sizeof(hash::THash) * hashes.size());
  } else {
    for (size_t i = 0; i < fiber_count_; ++i) {
      hasher.Absorb(*fibers_[i]);
    }
  }

  hasher.Absorb(memory_allocator_);

  return hasher.Hash();
}

size_t Checker::Random(size_t max) {
  static const size_t kRandomMaxLimit = 10;

  assert(max < kRandomMaxLimit);

  if (max == 0) {
    return 0;
  }
#if !defined(FORK_TRACE)
  GetCurrentFiber()->SetRandom((int32_t)max);
  Yield();
#endif

  size_t result = GetCurrentFiber()->ConsumeRandomNumber();
  Yield();
  return result;
}

void Checker::ReportStats() const {
  std::cout << "Diameter " << depth_ << std::endl;
  states_.PrintStatistics();
  std::cout << "Pruned " << pruned_count_ << std::endl;
  std::cout << "Elapsed " << timer_.Elapsed().count() / 1000000.0 << " ms"
            << std::endl;
  std::cout << std::endl;
}

void Checker::AddInvariant(Invariant inv) {
  CheckerGuard guard;
  invariants_.push_back(inv);
}

void Checker::AddPruneRule(PruneRule rule) {
  CheckerGuard guard;
  prune_rules_.push_back(std::move(rule));
}

void Checker::SetTracePath(const std::string& path) {
  CheckerGuard g;
  trace_fpath_.emplace(path);

#if defined(FORK_TRACE)
  tracer_.SetTrace(ReadViolationTrace());
#endif
}

void Checker::Fork() {
  if (!IsRunningInChecker() || forks_disabled_) {
    return;
  }
  Fiber* caller = GetCurrentFiber();
  caller->SetState(FiberState::Runnable);
  SwitchToChecker();
}

void Checker::SetDepth(size_t depth) {
  max_depth_ = depth;
}

CheckerGuard::CheckerGuard() {
  fiber_ = GetAndResetCurrentFiber();
}

CheckerGuard::~CheckerGuard() {
  SetCurrentFiber(fiber_);
}

ForkGuard::ForkGuard(bool forks_disabled) {
  prev_disabled_ = GetCurrentChecker()->DisableForks(forks_disabled);
}

ForkGuard::~ForkGuard() {
  GetCurrentChecker()->DisableForks(prev_disabled_);
}

////////////////////////////////////////////////////////////////////////////////

FiberId GetThisFiberId() {
  return GetCurrentFiber()->Id();
}

void Spawn(FiberRoutine routine) {
  CheckerGuard guard;
  auto r = new FiberRoutine(routine);  // todo: fix leak
  GetCurrentChecker()->Spawn(r);
  // Fork();
}

size_t Random(size_t max) {
  return GetCurrentChecker()->Random(max);
}

void Fork() {
  GetCurrentChecker()->Fork();
}

}  // namespace fork
