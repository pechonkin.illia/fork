#include <fork/core/trace/frame.hpp>

#include <wheels/support/fork.hpp>

#include <iostream>
#include <sstream>
#include <memory>

#include <sys/types.h>
#include <unistd.h>

namespace trace {

// tiny::support copy-paste ¯\_(ツ)_/¯
struct Quoted {
  std::string_view str_;
};

std::ostream& operator<<(std::ostream& out, const Quoted& q) {
  out << '\'' << q.str_ << '\'';
  return out;
}

Quoted Quoted(const std::string_view str) {
  return {str};
}

class StringStreamConsumer : public wheels::IByteStreamConsumer {
 public:
  StringStreamConsumer(std::stringstream& os) : stream_(os) {
  }

  void Consume(std::string_view chunk) override {
    stream_.write(chunk.data(), chunk.size());
  }

  void HandleEof() override {
    // nothing
  }

 private:
  std::stringstream& stream_;
};

class DebuggerExecutor {
 public:
  DebuggerExecutor() {
    std::string debugger = "gdb -quiet  -batch -annotate 3";
    pid_t pid = getpid();
    os_ << debugger << " attach " << pid << " ";
    os_ << ex_ << "'set print elements 10' ";
  }

  void Locals(const std::string& func) {
    os_ << ex_ << Quoted("frame function " + func);
    Begin(func);
    os_ << ex_ << Quoted(" info locals ");
    // os_ << ex_ << Quoted(" info args ");
    End(func);
  }

  std::string String() {
    return os_.str();
  }

 private:
  void Begin(const std::string& cmd) {
    os_ << ex_ << Quoted(echo_ + "\\nbegin " + cmd + "\\n\\n");
  }

  void End(const std::string& cmd) {
    os_ << ex_ << Quoted(echo_ + "\\nend " + cmd + "\\n\\n");
  }

 private:
  std::ostringstream os_;
  std::string ex_ = " -ex ";
  std::string echo_ = " echo ";
};

std::vector<std::string> ConvertToFrames(std::stringstream& is) {
  std::vector<std::string> frames;
  bool is_frame = false;
  std::stringstream os;
  for (std::string line; std::getline(is, line);) {
    std::string begin("begin");
    std::string end("end");

    auto result_end = std::mismatch(end.begin(), end.end(), line.begin());
    if (result_end.first == end.end()) {
      is_frame = false;
      frames.emplace_back(os.str());

      os.str("");
      os.clear();
    }

    if (is_frame && !line.empty()) {
      os << line << std::endl;
    }

    auto result_begin = std::mismatch(begin.begin(), begin.end(), line.begin());
    if (result_begin.first == begin.end()) {
      is_frame = true;
    }
  }
  return frames;
}

std::vector<std::string> LoadDebuggerInfo(
    const std::vector<std::string>& functions) {
  DebuggerExecutor ex;

  for (auto& f : functions) {
    ex.Locals(f);
  }

  auto target = [&ex]() { system(ex.String().c_str()); };

  std::stringstream out;
  ExecuteWithFork(target, std::make_unique<StringStreamConsumer>(out), nullptr);

  return ConvertToFrames(out);
}

}  // namespace trace
