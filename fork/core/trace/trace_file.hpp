#pragma once

#include <fork/core/trace/trace.hpp>

#include <string>

namespace trace {

std::string TraceFile(std::string_view test_name);

void WriteTrace(const std::string& file_path, Trace trace);
Trace ReadTrace(const std::string& file_path);

}  // namespace trace
