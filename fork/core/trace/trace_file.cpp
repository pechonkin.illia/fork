#include <fork/core/trace/trace_file.hpp>

#include <fork/core/support/fs.hpp>

#include <wheels/support/env.hpp>

#include <sstream>
#include <fstream>

namespace trace {

std::string TraceFile(std::string_view test_name) {
  std::ostringstream out;
  out << wheels::GetTempPath().value_or("/tmp") << "/fork/tests/" << test_name
      << ".trace";
  return out.str();
}

void WriteTrace(const std::string& file_path, Trace trace) {
  fork::support::CreateDirectories(file_path);

  std::ofstream out(file_path);
  for (auto& step : trace) {
    out << (int)step.fiber_index << " " << (int)step.random_number << " ";
  }
  out.close();
}

static Trace LoadTrace(std::istream& input) {
  Trace trace;

  int fiber_index;
  int random_number;
  while (input >> fiber_index) {
    input >> random_number;
    trace.push_back({(int8_t)fiber_index, (int8_t)random_number});
  }

  return trace;
}

Trace ReadTrace(const std::string& file_path) {
  std::ifstream file(file_path);
  return LoadTrace(file);
}

}  // namespace trace
