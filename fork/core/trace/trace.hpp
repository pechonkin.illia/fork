#pragma once

#include <vector>
#include <cstdlib>
#include <cassert>

namespace trace {

struct Step {
  int8_t fiber_index;
  int8_t random_number;
};

inline Step JustStep(size_t fiber_index) {
  assert(fiber_index < 128);
  return {(int8_t)fiber_index, -1};
}

inline Step RandomStep(size_t fiber_index, size_t random_number) {
  assert(fiber_index < 128);
  assert(random_number < 128);
  return {(int8_t)fiber_index, (int8_t)random_number};
}

using Trace = std::vector<Step>;

}  // namespace trace
