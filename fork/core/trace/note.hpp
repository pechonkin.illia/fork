#pragma once

#include <string>

#include <wheels/support/string_builder.hpp>

namespace fork {

void ShowNote(const std::string& note, bool internal = false);

}  // namespace fork

#if defined(FORK_TRACE)

#define __FORK_NOTE_IMPL(ctx, note, internal)                             \
  do {                                                                    \
    fork::ShowNote(wheels::StringBuilder() << '[' << ctx << "] " << note, \
                   internal);                                             \
  } while (false)

#define FORK_NOTE(ctx, note) __FORK_NOTE_IMPL(ctx, note, false)

#define FORK_INTERNAL_NOTE(ctx, note) __FORK_NOTE_IMPL(ctx, note, true)

#else

#define FORK_NOTE(ctx, note)

#define FORK_INTERNAL_NOTE(ctx, note)

#endif