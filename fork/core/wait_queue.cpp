#include <fork/core/wait_queue.hpp>

#include <fork/core/fiber.hpp>
#include <fork/core/checker.hpp>

#include <fork/core/trace/note.hpp>

#include <fork/test/random.hpp>

namespace fork {

// Shortcuts

static void Suspend(WaitQueue* wq) {
  GetCurrentChecker()->Suspend(wq);
}

static FiberPtrs GetWaiters(const WaitQueue* wq) {
  return GetCurrentChecker()->GetWaiters(wq);
}

static void Resume(Fiber* fiber) {
  GetCurrentChecker()->Resume(fiber);
}

// WaitQueue

void WaitQueue::Park() {
  Suspend(this);
  FORK_INTERNAL_NOTE("WaitQueue",
                     "resumed by thread T" << GetCurrentFiber()->Initiator());
}

void WaitQueue::WakeOne() {
  FiberPtrs fibers = GetWaiters(this);
  if (fibers.empty()) {
    return;
  }
  size_t index = Random(fibers.size() - 1);
  fibers[index]->Initiator() = GetThisFiberId();
  Resume(fibers[index]);

  FORK_INTERNAL_NOTE("WaitQueue", "wake thread T" << fibers[index]->Id()
                                                  << " (waiters: " << ToString()
                                                  << ")");
}

void WaitQueue::WakeAll() {
  FORK_INTERNAL_NOTE("WaitQueue", "wake all waiters: " << ToString());

  FiberPtrs waiters = GetWaiters(this);
  for (Fiber* f : waiters) {
    f->Initiator() = GetThisFiberId();
    Resume(f);
  }
}

std::string WaitQueue::ToString() const {
  FiberPtrs waiters = GetWaiters(this);

  if (waiters.empty()) {
    return "<empty>";
  }

  std::stringstream out;
  out << "{";
  for (size_t i = 0; i < waiters.size(); ++i) {
    out << 'T' << waiters[i]->Id();
    if (i + 1 < waiters.size()) {
      out << ", ";
    } else {
      out << "}";
    }
  }
  return out.str();
}

}  // namespace fork
