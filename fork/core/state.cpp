#include <fork/core/state.hpp>

#include <cstring>

namespace fork {

size_t StatePath::Length() const {
  return storage_.Size() / sizeof(Step);
}

StatePath::Storage StatePath::Allocate(size_t length) {
  size_t size = length * sizeof(Step);
  return {storage::DiskAllocate(size), size};
}

StatePath StatePath::Append(Step step) const {
  size_t length = Length();

  // Allocate storage
  Storage new_storage = Allocate(length + 1);
  // Copy this path
  memcpy(new_storage.Data(), storage_.Data(), storage_.Size());
  // Append new step
  Step* steps = (Step*)new_storage.Data();
  steps[length] = step;

  return {new_storage};
}

std::vector<trace::Step> StatePath::ToVector() const {
  std::vector<Step> path;

  size_t length = Length();
  path.reserve(length);

  Step* steps = (Step*)storage_.Data();
  for (size_t i = 0; i < Length(); ++i) {
    path.push_back(steps[i]);
  }

  return path;
}

State State::Create(const FiberPtrs& fibers, size_t existing_fiber_count,
                    const MemoryAllocator& allocator, StatePath path) {
  State state;

  state.fiber_snapshots_ = reinterpret_cast<FiberSnapshots>(
      storage::DiskAllocate(sizeof(Fiber::Snapshot) * existing_fiber_count));
  for (size_t i = 0; i < existing_fiber_count; ++i) {
    state.fiber_snapshots_[i] = fibers[i]->CreateSnapshot();
  }

  state.fiber_count_ = existing_fiber_count;
  state.allocator_snapshot_ = allocator.CreateSnapshot();
  state.path_ = path;

  return state;
}

}  // namespace fork
