#pragma once

#include <fork/core/fiber.hpp>
#include <fork/core/memory/allocator.hpp>
#include <fork/core/trace/trace.hpp>

#include <wheels/support/intrusive_list.hpp>
#include <wheels/logging/logging.hpp>

#include <functional>
#include <cassert>

namespace fork {

struct StatePath {
  using Step = trace::Step;
  using Storage = wheels::MemSpan;

  StatePath() : storage_(nullptr, 0) {
  }

  StatePath Append(Step step) const;
  size_t Length() const;

  std::vector<Step> ToVector() const;

 private:
  StatePath(wheels::MemSpan storage) : storage_(storage) {
  }

  static Storage Allocate(size_t length);

 private:
  Storage storage_;
};

// Doesn't hold ownership. rename to StateView?
class State : public wheels::IntrusiveListNode<State> {
 public:
  using FiberSnapshots = Fiber::Snapshot*;

  static State Create(const FiberPtrs& fibers, size_t existing_fiber_count,
                      const MemoryAllocator& allocator, StatePath path);

  bool Enabled(size_t index) const {
    return index < fiber_count_ && fiber_snapshots_[index].Enabled();
  }

  size_t FiberCount() const {
    return fiber_count_;
  }

  const FiberSnapshots& Fibers() const {
    return fiber_snapshots_;
  }

  const MemoryAllocator::Snapshot& AllocatorSnapshot() const {
    return allocator_snapshot_;
  }

  const StatePath& Path() const {
    return path_;
  }

  size_t Depth() const {
    return path_.Length();
  }

  StatePath NewPath(trace::Step step) const {
    return path_.Append(step);
  }

 private:
  FiberSnapshots fiber_snapshots_;
  size_t fiber_count_{0};
  MemoryAllocator::Snapshot allocator_snapshot_;
  StatePath path_;
};

}  // namespace fork
