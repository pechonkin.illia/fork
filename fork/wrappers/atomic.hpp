#pragma once

#include <fork/core/trace/note.hpp>

#include <fork/core/wait_queue.hpp>

#include <fork/test/fork.hpp>

#include <atomic>

namespace fork {

namespace detail {

template <typename T>
T CutValue(T value) {
  return value;
}

template <>
size_t CutValue(size_t value);

template <typename T>
class AtomicValue {
 public:
  AtomicValue() = default;
  explicit AtomicValue(T value) : value_(CutValue(value)) {
  }

  void Store(T value) {
    value_ = CutValue(value);
  }

  T Load() const {
    return value_;
  }

  T Exchange(T new_value) {
    return std::exchange(value_, CutValue(new_value));
  }

  bool CompareExchange(T& expected, T desired) {
    desired = CutValue(desired);
    if (value_ == expected) {
      value_ = desired;
      return true;
    }
    expected = desired;
    return false;
  }

  T FetchAdd(T diff) {
    return std::exchange(value_, CutValue(value_ + diff));
  }

  T FetchSub(T diff) {
    return std::exchange(value_, CutValue(value_ - diff));
  }

 private:
  T value_;
};

}  // namespace detail

template <typename T>
class Atomic {
 public:
  Atomic() {
  }

  explicit Atomic(T initial_value) : impl_(initial_value) {
  }

  // NOLINTNEXTLINE
  T load() const {
    Fork();
    T value = impl_.Load();
    FORK_INTERNAL_NOTE("Atomic", "load / value = " << value);
    return value;
  }

  operator T() const noexcept {
    return load();
  }

  // NOLINTNEXTLINE
  void store(T value,
             std::memory_order /*unused*/ = std::memory_order_seq_cst) {
    Fork();
    impl_.Store(value);
    FORK_INTERNAL_NOTE("Atomic", "store / value = " << value);
#if defined(DOUBLE_FORK)
    Fork();
#endif
  }

  Atomic& operator=(T value) {
    store(value);
    return *this;
  }

  // NOLINTNEXTLINE
  T exchange(T new_value,
             std::memory_order /*order*/ = std::memory_order_seq_cst) {
    Fork();
    const T prev_value = impl_.Exchange(new_value);
#if defined(DOUBLE_FORK)
    Fork();
#endif
    FORK_INTERNAL_NOTE("Atomic", "exchange / prev = " << prev_value);
    return prev_value;
  }

  // NOLINTNEXTLINE
  bool compare_exchange_weak(T& expected, T desired,
                             std::memory_order /*unused*/,
                             std::memory_order /*unused*/) {
    Fork();
    bool succeeded = impl_.CompareExchange(expected, desired);
#if defined(DOUBLE_FORK)
    Fork();
#endif
    FORK_INTERNAL_NOTE("Atomic",
                       "compare_exchange_weak / succeeded = " << succeeded);
    return succeeded;
  }

  // NOLINTNEXTLINE
  bool compare_exchange_weak(
      T& expected, T desired,
      std::memory_order order = std::memory_order_seq_cst) {
    return compare_exchange_weak(expected, desired, order, order);
  }

  // NOLINTNEXTLINE
  bool compare_exchange_strong(T& expected, T desired,
                               std::memory_order /*unused*/,
                               std::memory_order /*unused*/) {
    Fork();
    bool succeeded = impl_.CompareExchange(expected, desired);
#if defined(DOUBLE_FORK)
    Fork();
#endif
    FORK_INTERNAL_NOTE("Atomic",
                       "compare_exchange_strong / succeeded = " << succeeded);
    return succeeded;
  }

  // NOLINTNEXTLINE
  bool compare_exchange_strong(
      T& expected, T desired,
      std::memory_order order = std::memory_order_seq_cst) noexcept {
    return compare_exchange_strong(expected, desired, order, order);
  }

  // NOLINTNEXTLINE
  T fetch_add(const T value,
              std::memory_order /*unused*/ = std::memory_order_seq_cst) {
    Fork();
    T prev_value = impl_.FetchAdd(value);
#if defined(DOUBLE_FORK)
    Fork();
#endif
    FORK_INTERNAL_NOTE("Atomic", "fetch_add / prev = " << prev_value);
    return prev_value;
  }

  // NOLINTNEXTLINE
  T fetch_sub(const T value,
              std::memory_order /*unused*/ = std::memory_order_seq_cst) {
    Fork();
    T prev_value = impl_.FetchSub(value);
#if defined(DOUBLE_FORK)
    Fork();
#endif
    FORK_INTERNAL_NOTE("Atomic", "fetch_sub / prev = " << prev_value);
    return prev_value;
  }

  T operator++() {
    return fetch_add(1) + 1;
  }

  T operator--() {
    return fetch_sub(1) - 1;
  }

  // wait / notify

  // NOLINTNEXTLINE
  void wait(T old) {
    do {
      Fork();
      FutexWait(old);
    } while (load() == old);  // One more fork here!
  }

  // NOLINTNEXTLINE
  void notify_one() {
    Fork();
    futex_.WakeOne();
  }

  // NOLINTNEXTLINE
  void notify_all() {
    Fork();
    futex_.WakeAll();
  }

 private:
  void FutexWait(T old) {
    if (impl_.Load() == old) {
      futex_.Park();
    }
  }

 private:
  detail::AtomicValue<T> impl_;
  WaitQueue futex_{"atomic"};
};

}  // namespace fork
