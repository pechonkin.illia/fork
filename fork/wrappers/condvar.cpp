#include <fork/wrappers/condvar.hpp>

#include <fork/core/checker.hpp>
#include <fork/core/wait_queue.hpp>
#include <fork/core/trace/note.hpp>

#include <fork/test/fork.hpp>

namespace fork {

/////////////////////////////////////////////////////////////////////

class ConditionVariable::Impl {
 public:
  void Wait(Lock& lock) {
    // Spurious wakeup
    if (!GetCurrentChecker()->ForksDisabled() && SpuriousWakeup()) {
      FORK_INTERNAL_NOTE("CondVar", "spurious wakeup");
      return;
    }

    // ShowNote("Going to park in condvar");

    Fork();
    FORK_ATOMICALLY {
      lock.unlock();
    }
    wait_queue_.Park();

    lock.lock();

    FORK_INTERNAL_NOTE("CondVar", "wait completed, lock re-acquired");
  }

  void NotifyOne() {
    Fork();
    wait_queue_.WakeOne();
  }

  void NotifyAll() {
    Fork();
    wait_queue_.WakeAll();
  }

 private:
  bool SpuriousWakeup() {
    return Random(1) == 1;
  }

 private:
  WaitQueue wait_queue_{"condvar"};
};

/////////////////////////////////////////////////////////////////////

ConditionVariable::ConditionVariable()
    : pimpl_(std::make_unique<ConditionVariable::Impl>()) {
}

ConditionVariable::~ConditionVariable() {
}

void ConditionVariable::wait(Lock& lock) {  // NOLINT
  pimpl_->Wait(lock);
}

void ConditionVariable::notify_one() {  // NOLINT
  pimpl_->NotifyOne();
}

void ConditionVariable::notify_all() {  // NOLINT
  pimpl_->NotifyAll();
}

}  // namespace fork
