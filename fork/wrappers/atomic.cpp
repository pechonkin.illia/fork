#include <fork/wrappers/atomic.hpp>

#include <fork/test/checker.hpp>

namespace fork {

static size_t bound = 0;

CheckerView& CheckerView::SetUIntAtomicBound(size_t value) {
  bound = value;
  return *this;
}

namespace detail {

template <>
size_t CutValue(size_t value) {
  if (bound == 0) {
    return value;
  }
  return (value + bound) % bound;
}

}  // namespace detail

}  // namespace fork
