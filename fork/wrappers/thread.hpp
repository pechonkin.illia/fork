#pragma once

#include <memory>
#include <functional>

namespace fork {

using ThreadRoutine = std::function<void()>;

class Thread {
 public:
  Thread(ThreadRoutine routine);
  Thread(Thread&& other);
  ~Thread();

  void join();  // NOLINT

  bool joinable();  // NOLINT

  void detach();  // NOLINT

 private:
  class Impl;
  std::unique_ptr<Impl> pimpl_;
};

void ThisThreadYield();

using ThreadId = size_t;
ThreadId ThisThreadId();

}  // namespace fork
