#pragma once

#include <fork/core/wait_queue.hpp>
#include <fork/core/checker.hpp>

#include <fork/test/trace.hpp>

namespace fork {

class Event {
 public:
  Event(std::string descr) : wait_queue_(std::move(descr)) {
  }

  void Await() {
    if (!ready_) {
      wait_queue_.Park();
    }
  }

  void Signal() {
    ready_ = true;
    FORK_INTERNAL_NOTE("Event", "signaled");
    wait_queue_.WakeAll();
  }

 private:
  bool ready_{false};
  WaitQueue wait_queue_;
};

}  // namespace fork
