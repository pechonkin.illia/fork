#include <fork/wrappers/event.hpp>
#include <fork/wrappers/thread.hpp>

#include <fork/core/checker.hpp>

#include <fork/test/test.hpp>

#include <wheels/support/panic.hpp>

namespace fork {

/////////////////////////////////////////////////////////////////////

void ThisThreadYield() {
  GetCurrentChecker()->Yield();
}

ThreadId ThisThreadId() {
  return GetThisFiberId();
}

class Thread::Impl {
 public:
  Impl(FiberRoutine routine) : routine_(std::move(routine)) {
    Spawn([this]() { Run(); });
  }

  ~Impl() {
    FORK_ASSERT(joined_, "Not joined");
  }

  void Join() {
    FORK_INTERNAL_NOTE("Thread", "joining");
    if (joined_) {
      FORK_INTERNAL_NOTE("Thread", "joined");
      return;  // already
    }
    done_.Await();
    FORK_INTERNAL_NOTE("Thread", "joined");
    joined_ = true;
  }

  bool Joinable() const {
    return true;
  }

 private:
  void Run() {
#if defined(FORK_TRACE)
    id_ = GetThisFiberId();
#endif

    GetCurrentFiber()->CanonizeValues({(char*)this, sizeof(*this)});
    routine_();
    done_.Signal();
  }

 private:
  FiberRoutine routine_;
  bool joined_{false};
  Event done_{"thread::join"};

#if defined(FORK_TRACE)
  FiberId id_;
#endif
};

/////////////////////////////////////////////////////////////////////

Thread::Thread(ThreadRoutine routine)
    : pimpl_(std::make_unique<Thread::Impl>(std::move(routine))) {
}

Thread::Thread(Thread&& other) : pimpl_(std::move(other.pimpl_)) {
}

Thread::~Thread() {
}

void Thread::join() {  // NOLINT
  pimpl_->Join();
}

bool Thread::joinable() {  // NOLINT
  return pimpl_->Joinable();
}

void Thread::detach() {  // NOLINT
  WHEELS_PANIC("Not supported");
}

}  // namespace fork
