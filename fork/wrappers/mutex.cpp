#include <fork/wrappers/mutex.hpp>

#include <fork/core/checker.hpp>
#include <fork/core/wait_queue.hpp>
#include <fork/core/trace/note.hpp>

#include <fork/test/fork.hpp>

namespace fork {

/////////////////////////////////////////////////////////////////////

class Mutex::Impl {
 public:
  void Lock() {
    Fork();

    while (locked_) {
      FORK_INTERNAL_NOTE("Mutex", "aready locked by thread T" << owner_id_);
      wait_queue_.Park();
    }
    locked_ = true;

    FORK_INTERNAL_NOTE("Mutex", "acquired");

#if defined(FORK_TRACE)
    owner_id_ = GetThisFiberId();
#endif
  }

  bool TryLock() {
    Fork();

    if (!locked_) {
      locked_ = true;
#if defined(FORK_TRACE)
      owner_id_ = GetThisFiberId();
#endif
      FORK_INTERNAL_NOTE("Mutex", "acquired by TryLock");
      return true;
    }

    FORK_INTERNAL_NOTE(
        "Mutex", "TryLock failed, already locked by thread T" << owner_id_);
    return false;
  }

  void Unlock() {
    Fork();

    locked_ = false;

#if defined(FORK_TRACE)
    owner_id_ = kInvalidFiberId;
#endif

    wait_queue_.WakeOne();

    FORK_INTERNAL_NOTE("Mutex", "released");
  }

 private:
  bool locked_{false};
  WaitQueue wait_queue_{"mutex"};

#if defined(FORK_TRACE)
  FiberId owner_id_{kInvalidFiberId};
#endif
};

/////////////////////////////////////////////////////////////////////

Mutex::Mutex() : pimpl_(std::make_unique<Mutex::Impl>()) {
}

Mutex::~Mutex() {
}

void Mutex::lock() {  // NOLINT
  pimpl_->Lock();
}

bool Mutex::try_lock() {  // NOLINT
  return pimpl_->TryLock();
}

void Mutex::unlock() {  // NOLINT
  pimpl_->Unlock();
}

}  // namespace fork
