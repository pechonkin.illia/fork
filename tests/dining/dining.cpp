#include "dining.hpp"

#include <fork/test/test.hpp>

namespace dining {

void Plate::Access() {
  FORK_ASSERT(!accessed_.exchange(true, std::memory_order_relaxed),
                 "Mutual exclusion violated");
  FORK_ASSERT(accessed_.exchange(false, std::memory_order_relaxed),
                "Mutual exclusion violated");
}

}  // namespace dining
