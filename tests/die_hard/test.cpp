#include <fork/stdlike/mutex.hpp>
#include <fork/stdlike/thread.hpp>

#include <fork/test/test.hpp>
#include <fork/test/trace.hpp>
#include <fork/test/fork.hpp>
#include <fork/test/checker.hpp>

using namespace fork::stdlike;

FORK_TEST(DieHard) {
  static const size_t kWant = 4;
  static const size_t kBigCapacity = 5;
  static const size_t kSmallCapacity = 3;

  size_t big = 0;
  size_t small = 0;

  fork::GetChecker()
      .AddInvariant([&]() -> std::pair<bool, std::string> {
        return {big != kWant, "Solved"};
      })
      .FailsExpected();

  auto loop = [&](std::function<void()> step) {
    while (true) {
      step();
      fork::Fork();
    }
  };

  auto fill_big = [&]() { big = kBigCapacity; };

  auto fill_small = [&]() { small = kSmallCapacity; };

  auto empty_big = [&]() { big = 0; };

  auto empty_small = [&]() { small = 0; };

  auto small_to_big = [&]() {
    size_t delta = std::min((kBigCapacity - big), small);
    big += delta;
    small -= delta;
  };

  auto big_to_small = [&]() {
    size_t delta = std::min((kSmallCapacity - small), big);
    small += delta;
    big -= delta;
  };

  std::vector<thread> threads;

  threads.emplace_back([&]() { loop(fill_big); });
  threads.emplace_back([&]() { loop(fill_small); });

  threads.emplace_back([&]() { loop(empty_big); });
  threads.emplace_back([&]() { loop(empty_small); });

  threads.emplace_back([&]() { loop(small_to_big); });
  threads.emplace_back([&]() { loop(big_to_small); });

  for (auto& th : threads) {
    th.join();
  }
}
