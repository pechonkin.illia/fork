#include <fork/stdlike/mutex.hpp>
#include <fork/stdlike/thread.hpp>

#include <fork/test/test.hpp>
#include <fork/test/checker.hpp>

#include <cmath>
#include <string>

#include "channel.hpp"

using namespace fork::stdlike;

FORK_TEST(LostWakeup) {
  fork::GetChecker()
      .ExpectDeadlock()
      .ShowLocals(false);

  static const size_t kItems = 2;
  Channel<int> items;

  auto consume = [&]() { items.Take(); };
  std::vector<thread> consumers;
  for (size_t i = 0; i < kItems; ++i) {
    consumers.emplace_back(consume);
  }

  thread producer([&]() {
    for (size_t i = 0; i < kItems; ++i) {
      items.Put(42);
    }
  });

  producer.join();
  for (auto& consumer : consumers) {
    consumer.join();
  }
}
