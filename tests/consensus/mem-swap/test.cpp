#include <fork/stdlike/mutex.hpp>
#include <fork/stdlike/thread.hpp>
#include <fork/stdlike/atomic.hpp>

#include <fork/test/test.hpp>
#include <fork/test/checker.hpp>
#include <fork/test/fork.hpp>

#include <cmath>
#include <string>

using namespace fork::stdlike;

template <typename T>
void MemSwap(atomic<T>& left, atomic<T>& right) {
  fork::Fork();
  fork::ForkGuard guard;
  T to_right = left.load();
  left.store(right.load());
  right.store(to_right);
}

FORK_TEST(Memswap) {
  fork::GetChecker()
      .ShowLocals(false);
  // todo: fork::HashOff

  // Consensus objects

  static const size_t kThreads = 4;
  std::array<int, kThreads> generated;
  auto generator = std::hash<int>();
  for (size_t i = 0; i < kThreads; ++i) {
    generated[i] = generator(i);
    FORK_NOTE("generated: " << generated[i]);
  }

  std::array<atomic<int>, kThreads> decisions;
  {
    fork::ForkGuard guard;
    for (auto& decision : decisions) {
      decision.store(-1);
    }
  }

  // User objects

  std::array<atomic<int>, kThreads> inputs;
  {
    fork::ForkGuard guard;
    for (auto& val : inputs) {
      val.store(-1);
    }
  }

  std::array<atomic<int>, kThreads> ids;
  {
    fork::ForkGuard guard;
    for (size_t i = 0; i < kThreads; ++i) {
      ids[i].store(i);
    }
  }

  atomic<int> rendezvous{-1};

  auto decide = [&](int value, size_t id) {
    inputs[id].store(value);
    FORK_NOTE("value: " << value);

    MemSwap(ids[id], rendezvous);

    if (ids[id].load() == -1) {
      decisions[id].store(value);
      return;
    } else {
      for (size_t i = 0; i < kThreads; ++i) {
        if (ids[i].load() == -1) {
          decisions[id].store(inputs[i].load());
          return;
        }
      }
    }

    FORK_ASSERT(false, "unreachable");
  };

  std::vector<thread> consumers;
  for (size_t i = 0; i < kThreads; ++i) {
    FORK_NOTE("v0: " << generated[i]);
    consumers.emplace_back(std::bind(decide, generated[i], i));
  }

  for (auto& consumer : consumers) {
    consumer.join();
  }

  // Check for validity, agreement

  fork::ForkGuard guard;

  int first_decision = decisions[0].load();

  auto it = std::find(generated.begin(), generated.end(), first_decision);
  FORK_ASSERT(it != generated.end(), "validity violated");

  for (auto& d : decisions) {
    FORK_ASSERT(d.load() == first_decision, "agreement violated");
  }
}
