#pragma once

#include <tinyfutures/executors/thread_pool.hpp>

namespace executors {

// Fixed-size pool of threads + unbounded blocking queue
IThreadPoolPtr MakeStaticThreadPool(size_t threads);

}  // namespace executors
