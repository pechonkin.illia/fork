#pragma once

#include <tinyfutures/executors/task.hpp>

namespace executors {

void SafelyExecuteHere(Task& task);

}  // namespace executors
