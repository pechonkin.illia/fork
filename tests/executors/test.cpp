#include <fork/stdlike/mutex.hpp>

#include <fork/test/test.hpp>
#include <fork/test/trace.hpp>
#include <fork/test/checker.hpp>

#include <tinyfutures/executors/static_thread_pool.hpp>
#include <tinyfutures/executors/strand.hpp>

#include <cmath>
#include <string>

using namespace executors;

FORK_TEST(Strand) {
  fork::GetChecker()
      .FailsExpected();

  auto tp = MakeStaticThreadPool(1);
  auto strand = MakeStrand(tp);

  size_t tasks = 0;
  strand->Execute([&]() { ++tasks; });
  strand->Execute([&]() { ++tasks; });

  tp->Join();

  FORK_ASSERT(tasks == 2, "Expected 2 completed tasks, found " << tasks);
}
