#pragma once

#include <fork/stdlike/atomic.hpp>

#include <vector>

using namespace fork::stdlike;

struct Node {
  atomic<Node*> next_;
  bool free_{true};
  std::string label_;

  bool IsReleased() const {
    return free_;
  }

  void Release() {
    free_ = true;
  }

  void Acquire() {
    free_ = false;
  }

  void SetLabel(const std::string& name) {
    std::ostringstream os;
    os << name << " (" << std::hex << this << std::dec << ") ";
    label_ = os.str();
  }

  std::string GetLabel() const {
    return label_;
  }
};

class MemoryPool {
 public:
  MemoryPool(size_t count) {
    nodes_ = std::vector<Node>{count};
    char name = 'A';

    for (auto it = nodes_.rbegin(); it != nodes_.rend(); ++it) {
      it->SetLabel(std::string(1, name++));
    }
  }

  std::vector<Node>& GetNodes() {
    return nodes_;
  }

  trace::StateDescription DescribeState() const {
    trace::StateDescription description;
    for (auto& node : nodes_) {
      description.Add(node.GetLabel(), !node.free_);
    }
    return description;
  }

 private:
  std::vector<Node> nodes_;
};