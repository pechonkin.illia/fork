#include <fork/stdlike/atomic.hpp>
#include <fork/stdlike/thread.hpp>
#include <fork/stdlike/condition_variable.hpp>
#include <fork/stdlike/mutex.hpp>

#include <fork/test/test.hpp>
#include <fork/test/checker.hpp>

#include <cmath>

using namespace std::literals::chrono_literals;

using namespace fork::stdlike;

class ZeroAwaiter {
 public:
  void Up() {
    tokens_.fetch_add(1);
  }

  void Down() {
    tokens_.fetch_sub(1);
    reached_zero_.notify_one();
  }

  void Await() {
    std::unique_lock lock(mutex_);
    while (tokens_.load() != 0) {
      reached_zero_.wait(lock);
    }
  }

 private:
  condition_variable reached_zero_;
  mutex mutex_;
  atomic<size_t> tokens_{0};
};

FORK_TEST(LostWakeup) {
  ZeroAwaiter awaiter;

  fork::GetChecker()
      .ExpectDeadlock();

  thread producer([&awaiter]() {
    awaiter.Up();
    awaiter.Down();
  });

  thread waiter([&awaiter]() { awaiter.Await(); });

  waiter.join();
  producer.join();
}
