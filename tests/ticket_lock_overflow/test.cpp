#include <fork/stdlike/thread.hpp>

#include <fork/stdlike/atomic.hpp>
#include <fork/test/test.hpp>
#include <fork/test/checker.hpp>

#include <cmath>
#include <string>
#include <iostream>

#include "ticket_lock.hpp"

using namespace fork::stdlike;

FORK_TEST(AtomicOverflow) {
  TicketLock lock;
  bool in_critical_section = false;

  fork::GetChecker()
      .SetUIntAtomicBound(3)
      .FailsExpected()
      .PrintState(
      [&lock, &in_critical_section]() {
        trace::StateDescription d{"LockState"};
        d.Add("TicketLock", lock);
        d.Add("in_critical_section_", in_critical_section);
        return d;
      });

  auto routine = [&]() {
    while (true) {
      if (lock.TryLock()) {
        FORK_ASSERT(!std::exchange(in_critical_section, true),
                    "Mutual exclusion violated");
        FORK_ASSERT(std::exchange(in_critical_section, false),
                    "Mutual exclusion violated");
        lock.Unlock();
      }
    }
  };

  const static size_t kThreads = 2;
  std::vector<thread> threads;
  for (size_t i = 0; i < kThreads; ++i) {
    threads.emplace_back(routine);
  }

  for (auto& th : threads) {
    th.join();
  }
}
