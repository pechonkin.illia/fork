#include <fork/stdlike/thread.hpp>

#include <fork/test/test.hpp>
#include <fork/test/random.hpp>
#include <fork/test/checker.hpp>

#include <iostream>
#include <cmath>

#include "alloc_lfstack.hpp"
#include "memory.hpp"

using namespace fork::stdlike;

FORK_TEST(ABA) {
  MemoryPool pool{3};
  LFAllocator stack{pool};

  fork::GetChecker()
      .PrintState([&stack, &pool]() {
        trace::StateDescription d{"LFAlloc"};
        d.Add("LFStack", stack);
        d.Add("Memory", pool);
        return d;
      })
      .AddInvariant([&stack]() -> std::pair<bool, std::string> {
        auto* node = stack.Top();
        while (node != nullptr) {
          if (node->IsReleased()) {
            return {false, "Node from stack was released!"};
          }
          node = node->next_.load();
        }
        return {true, ""};
      })
      .FailsExpected();

  auto worker = [&stack, &pool]() {
    while (true) {
      size_t available = pool.ReleasedCount();
      int node_index = fork::Random(available) - 1;
      if (node_index == -1) {
        FORK_NOTE("Alloc(): before");
        stack.Alloc();
        FORK_NOTE("Alloc(): after");
      } else {
        FORK_NOTE("Free(): before");
        Node* to_free = pool.Acquire(node_index);
        stack.Free(to_free);
        FORK_NOTE("Free(): after");
      }
    }
  };

  std::vector<thread> threads;
  const static size_t kThreads = 3;
  for (size_t i = 0; i < kThreads; ++i) {
    threads.emplace_back(worker);
  }

  for (auto& th : threads) {
    th.join();
  }
}
